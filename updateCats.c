///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
//
// @file updateCats.c
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   22 Feb 2022
//////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "catDatabase.h"
#include "updateCats.h"

int updateCatName(int index, char newName[]) {
   if (strlen(newName) > 0) {
      for (int i = 0; i < MAX_CATS; i++) {
         if (names[i] == newName) {
            printf("Name not unique\n");
            return 1;
         }
      }
      strcpy(names[index], newName);
      return 0;
   }
   return 1;
}

void fixCat(int index) {
   isFixedArr[index] = true;
}

void updateCatWeight(int index, float newWeight) {
   if (newWeight > 0) {
      weights[index] = newWeight;
   }
}
