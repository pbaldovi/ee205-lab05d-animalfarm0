///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
//
// @file catDatabase.c
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   22 Feb 2022
//////////////////////////////////////////////////////////////////////////////

#include "catDatabase.h"

int currentCat = 0;

char names[MAX_CATS][NAME_LENGTH];
enum Gender genders[MAX_CATS];
enum Breed breeds[MAX_CATS];
bool isFixedArr[MAX_CATS];
float weights[MAX_CATS];
