///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
//
// @file deleteCats.c
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   22 Feb 2022
//////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include "catDatabase.h"
#include "deleteCats.h"

void deleteAllCats() {
   memset(names, 0, sizeof(names));
   memset(genders, 0, sizeof(names));
   memset(breeds, 0, sizeof(names));
   memset(isFixedArr, 0, sizeof(names));
   memset(weights, 0, sizeof(names));
   currentCat = 0;
}

void deleteCat(int index) {
   for (int i = index; i < MAX_CATS - 1; i++) {
      strcpy(names[i], names[i + 1]);
      genders[i] = genders[i + 1];
      breeds[i] = breeds[i + 1];
      isFixedArr[i] = isFixedArr[i + 1];
      weights[i] = weights[i + 1];
   }
}
