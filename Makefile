###############################################################################
#         University of Hawaii, College of Engineering
# @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
#
# @file    Makefile
# @version 1.0
#
# @author Paulo Baldovi <pbaldovi@hawaii.edu>
# @date   22 Feb 2022
#
# @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

TARGET = main


all:  $(TARGET)


CC     = gcc
CFLAGS = -Wall -Wextra $(DEBUG_FLAGS)


debug: DEBUG_FLAGS = -g -DDEBUG
debug: clean $(TARGET)

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

addCats.o: addCats.c addCats.h	
	$(CC) $(CFLAGS) -c addCats.c

reportCats.o: reportCats.c reportCats.h	
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h	
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c deleteCats.h	
	$(CC) $(CFLAGS) -c deleteCats.c

main.o: main.c addCats.h reportCats.h updateCats.h deleteCats.h catDatabase.h
	$(CC) $(CFLAGS) -c main.c


main: main.o addCats.o reportCats.o updateCats.o deleteCats.o catDatabase.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o addCats.o reportCats.o updateCats.o deleteCats.o catDatabase.o


test: $(TARGET)
	./$(TARGET)


clean:
	rm -f $(TARGET) *.o
