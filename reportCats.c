///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
//
// @file reportCats.h
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   22 Feb 2022
//////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "catDatabase.h"
#include "reportCats.h"

void printCat(int index) {
   if (index < 0 || index > MAX_CATS) {
      printf("animalFarm0; Bad cat [%d]\n", index);
   } else {
      printf("cat index = [%d] "
            "name=[%s] "
            "gender=[%d] " 
            "breed=[%d] "
            "isFixed=[%d] "
            "weight=[%f]\n", index, names[index], genders[index], breeds[index], isFixedArr[index], weights[index]);
   }
}

void printAllCats() {
   for (int i = 0; i < MAX_CATS; i++) {
      printf("cat index = [%d] "
            "name=[%s] "
            "gender=[%d] " 
            "breed=[%d] "
            "isFixed=[%d] "
            "weight=[%f]\n", i, names[i], genders[i], breeds[i], isFixedArr[i], weights[i]);
   }
}

int findCat(char name[]){
   for (int i = 0; i < MAX_CATS; i++) {
      if (names[i] == name) {
         return i;
      }
   }
   return 1;
}
