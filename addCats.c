///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
//
// @file addCats.c
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   22 Feb 2022
//////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include "catDatabase.h"
#include "addCats.h"

int addCat(char name[], enum Gender gender, enum Breed breed, bool isFixed, float weight) {
   for (int i = 0; i < MAX_CATS; i++) {
      if (names[i] == name) {
         printf("Name not unique\n");
         return 1;
      }
   }

   if (currentCat < MAX_CATS && strlen(name) != 0 && strlen(name) < NAME_LENGTH && weight > 0) {
      strcpy(names[currentCat], name);
      genders[currentCat] = gender;
      breeds[currentCat] = breed;
      isFixedArr[currentCat] = isFixed;
      weights[currentCat] = weight;
      currentCat++;
      return currentCat;
   } else {
      printf("Unable to add cat\n");
      return 1;
   }

}
