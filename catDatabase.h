///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
//
// @file catDatabase.h
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   22 Feb 2022
//////////////////////////////////////////////////////////////////////////////

#pragma once
#include <stdbool.h>

#define NAME_LENGTH     (30)
#define MAX_CATS        (30)

enum Gender {UNKNOWN_GENDER, MALE, FEMALE};
enum Breed {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};

extern int currentCat;
extern char names[MAX_CATS][NAME_LENGTH];
extern enum Gender genders[MAX_CATS];
extern enum Breed breeds[MAX_CATS];
extern bool isFixedArr[MAX_CATS];
extern float weights[MAX_CATS];

void initializeDatabase();
