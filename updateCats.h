///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
//
// @file updateCats.h
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   22 Feb 2022
//////////////////////////////////////////////////////////////////////////////

#pragma once

int updateCatName(int index, char newName[]);
void fixCat(int index);
void updateCatWeight(int index, float newWeight);
