///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
//
// @file reportCats.h
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   22 Feb 2022
//////////////////////////////////////////////////////////////////////////////

#pragma once

void printCat(int index);
void printAllCats();
int findCat(char name[]);
